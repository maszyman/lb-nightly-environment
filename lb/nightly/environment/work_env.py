import os
from contextlib import contextmanager
from venv import EnvBuilder
from subprocess import run
from urllib.request import urlopen
from importlib import resources


class CtxEnvBuilder(EnvBuilder):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.context = None

    def post_setup(self, context):
        self.context = context


def virtualenv(base_dir, requirements):
    """
    Creates the virtual environment
    """
    clear_env = os.environ.get("RESET_VENV")
    venv = CtxEnvBuilder(with_pip=True, clear=clear_env)
    venv.create(base_dir)

    pip_upgrade_cmd = [
        venv.context.env_exe,
        "-m",
        "pip",
        "install",
        "--upgrade",
        "pip",
    ]
    run(pip_upgrade_cmd, check=True)

    reqs = []
    with resources.open_text("lb.nightly.environment.data", requirements) as reqfile:
        reqs = reqfile.read().splitlines()

    if "OVERRIDE_PIP_REQUIREMENTS" in os.environ:
        for req in os.environ["OVERRIDE_PIP_REQUIREMENTS"].split():
            reqs.append(req)

    pip_install_cmd = [
        venv.context.env_exe,
        "-m",
        "pip",
        "install",
        *reqs,
    ]
    run(pip_install_cmd, check=True)
    return venv


def conda(base_dir, requirements):
    """
    Creates the conda environment
    """
    raise NotImplementedError()


@contextmanager
def work_env(base_dir, requirements):
    """
    Yields the environment to run the nightlies functions.
    If the requirements argument is `requirements.txt`,
    the virtualenv is yielded.
    If the requirements argument is `environment.yml`,
    the conda environment is yielded.
    Otherwise ValueError is raised.
    """
    if requirements == "requirements.txt":
        yield virtualenv(base_dir, requirements)
    elif requirements == "environment.yml":
        yield conda(base_dir, requirements)
    else:
        raise ValueError("The format of the requirements file is unknown")
